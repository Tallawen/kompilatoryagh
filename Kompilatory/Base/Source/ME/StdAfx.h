#ifndef __ME_BASE_STDAFX_H__
#define __ME_BASE_STDAFX_H__

#include "Config\Macros.h"
#include "Config\Comp.h"
#include "Config\Global.h"

#include <string>
#include <vector>
#include <algorithm>
#include <locale>
#include <cctype>
#include <stack>
#include <iostream>

#endif /*__ME_BASE_STDAFX_H__*/