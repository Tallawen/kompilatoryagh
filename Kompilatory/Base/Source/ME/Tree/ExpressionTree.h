#ifndef __ME_BASE_TREE_EXPRESSIONTREE_H__
#define __ME_BASE_TREE_EXPRESSIONTREE_H__

#include "../StdAfx.h"

#include "Node.h"

#include "../Tokens/Entities/TNumber.h"
#include "../Tokens/Entities/TOperator.h"

ME_BEGIN_NAMESPACE_BASE

namespace Tree {

    class ExpressionTree {
    private:
        TokensVector _tokens;

        std::stack<Node*> _stack; /* Stos odno�nie warto�ci */
        std::stack<Node*> _opStack; /* Stos odno�nie operator�w */

        Node *_root;

    public:

        /**
         * @fn  ExpressionTree::ExpressionTree(TokensVector tokens)
         *
         * @brief   Konstruktor
         */
        ExpressionTree(TokensVector tokens) : _tokens(tokens) {
        }

    public:       

        /**
         * @fn  void ExpressionTree::build();
         *
         * @brief   Metoda buduj�ca drzewo
         */
        void build() {
            if (_tokens.empty())
                return;

            Node *node = nullptr;
            
            for each (auto item in _tokens) {
                if (item == nullptr)
                    throw "TO jest nullem tutak -.-";

                node = new Node(item);

                if (!node->isValueOperator()) {
                    _stack.push(node);
                }

                if (node->isValueOperator()) {
                    if (!_opStack.empty()) {
                        if (node->value()->priority() < _opStack.top()->value()->priority()) { /* Kolejno�c */
                            joinOp();
                        }
                    }

                    _opStack.push(node);
                }
            }

            while (!_opStack.empty())
                if(!joinOp())
					throw "Nieprawidlowa ilosc operatorow do laczenia";  

            _root = _stack.top();
        }

        /**
         * @fn  int ExpressionTree::calculate(Node *node)
         *
         * @brief   Oblicza wyra�enie
         */
        int calculate(Node *node) {
            if (node == nullptr)
                return 0;

            if (node->isValueNumber())
                return dynamic_cast<Tokens::Entities::TNumber*>(node->value())->value();
            
            if (node->isValueOperator()) {
                Tokens::Entities::TOperator *op = dynamic_cast<Tokens::Entities::TOperator*>(node->value());

                if (op->arguments() == 1)
                    return calculate(node->left);
                else if (op->arguments() == 2)
                    return op->calculate(calculate(node->left), calculate(node->right));
                else
                    throw "Waln��e� sie gdzie";
            }
        }

        /**
         * @fn  int ExpressionTree::calculate()
         *
         * @brief   Oblicza wyra�enie
         */
        int calculate() {
            return _stack.empty() ? 0 : calculate(_stack.top());
        }

        void inf(Node *s) {
            if (s == nullptr)
                return;

            if (s != nullptr && s->isValueNumber())
                std::cout << s->value()->toString() << " ";
            else {

                std::cout << s->value()->toString() << " ";
                inf(s->left);
                inf(s->right);
            }
        }

        void inf() {
            return inf(_root);
        }

		TokensVector getSequence(Node *s) {
			if (s == nullptr)
				return TokensVector();

			if (s != nullptr && s->isValueNumber()) {
				return TokensVector(1, s->value());
			}
			else {
				TokensVector res(1, s->value());
				TokensVector left = getSequence(s->left);
				TokensVector right = getSequence(s->right);
				res.insert(res.end(), left.begin(), left.end());
				res.insert(res.end(), right.begin(), right.end());
				return res;
			}
		}

		TokensVector getSequence() {
			TokensVector tv = getSequence(_root);
			std::reverse(tv.begin(), tv.end());
			return tv;
		}

    private:
        /**
        * @fn  void ExpressionTree::joinOp()
        *
        * @brief   Metoda ��cz�ca
        */
        bool joinOp() {
            if (_stack.size() < 2)
                return false;

            Node *node = _opStack.top(); _opStack.pop();

            Node *leftNode = _stack.top(); _stack.pop();
            Node *rightNode = _stack.top(); _stack.pop();

            node->left = leftNode;
            node->right = rightNode;

            _stack.push(node);
			return true;
        }
        
    };

}

ME_END_NAMESPACE_BASE

#endif __ME_BASE_TREE_EXPRESSIONTREE_H__