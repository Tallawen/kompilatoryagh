#ifndef __ME_BASE_TREE_NODE_H__
#define __ME_BASE_TREE_NODE_H__

#include "../StdAfx.h"

#include "../Tokens/ITokenBase.h"

ME_BEGIN_NAMESPACE_BASE

namespace Tree {

    class Node {
    private:
        Tokens::ITokenBase *_value;

    public:
        Node *left; /* Lewy syn */
        Node *right; /* Prawy syn */

    public:

        /**
         * @fn  Node::Node(Tokens::ITokenBase *value)
         *
         * @brief   Konstruktor
         */
        Node(Tokens::ITokenBase *value) : _value(value) {
            left = nullptr;
            right = nullptr;
        }

    public:

        /**
         * @fn  Tokens::ITokenBase* Node::value() const
         *
         * @brief   Zwracanie warto�ci
         */
        Tokens::ITokenBase* value() const { return _value; }   

        /**
         * @fn  bool Node::isValueOperator() const
         *
         * @brief   Metoda zwracaj�ca informacje czy warto�� w danym nodzie jest operatorem
         */
        bool isValueOperator() const { return _value != nullptr ? _value->isOperator() : false; }

        /**
         * @fn  bool Node::isValueNumber() const
         *
         * @brief   Metoda zwracaj�ca informacje czy warto�� w danym nodzie jest liczb�
         */
        bool isValueNumber() const { return _value != nullptr ? _value->isNumber() : false; }
    };

}

ME_END_NAMESPACE_BASE

#endif /*__ME_BASE_TREE_NODE_H__*/