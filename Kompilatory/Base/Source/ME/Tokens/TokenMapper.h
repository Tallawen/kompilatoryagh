#ifndef __ME_BASE_TOKENS_TOKENMAPPER_H__
#define __ME_BASE_TOKENS_TOKENMAPPER_H__

#include "../StdAfx.h"

#include "ITokenBase.h"

#include "Entities\TNumber.h"
#include "Entities\Operators\TAdd.h"
#include "Entities\Operators\TDiv.h"
#include "Entities\Operators\TMul.h"
#include "Entities\Operators\TSub.h"

ME_BEGIN_NAMESPACE_BASE

namespace Tokens {

    class TokenMapper {
    public:
        static ITokenBase* FromString(const std::string &str) {
            if (str == "+") {
                return new Entities::Operators::TAdd();

            }
            else if (str == "/") {
                return new Entities::Operators::TDiv();

            }
            else if (str == "*") {
                return new Entities::Operators::TMul();

            }
            else if (str == "-") {
                return new Entities::Operators::TSub();
            }
            
            if (!isNumber(str))
                return nullptr;

            return new Entities::TNumber(str);
        }

    private:
        static bool isNumber(const std::string &s) {
            return !s.empty() && std::find_if(s.begin(), s.end(), [](char c) { return !std::isdigit(c); }) == s.end();
        }
    };

}

ME_END_NAMESPACE_BASE

#endif /*__ME_BASE_TOKENS_TOKENMAPPER_H__*/