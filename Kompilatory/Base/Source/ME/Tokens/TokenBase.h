#ifndef __ME_BASE_TOKENS_TOKENBASE_H__
#define __ME_BASE_TOKENS_TOKENBASE_H__

#include "../StdAfx.h"

#include "ITokenBase.h"

ME_BEGIN_NAMESPACE_BASE

namespace Tokens {
        
    template<typename VType> class TokenBase : public ITokenBase {  
    private:
        VType _value; /* Warto�� jak� przechowuje token */

        std::string _symbol;

        int _priority;

    protected:

        /**
         * @fn  TokenBase::TokenBase(VType value)
         *
         * @brief   Konstruktor
         */
        TokenBase(VType value, const std::string &symbol = "", const int priority = 0) 
            : _value(value), _symbol(symbol), _priority(priority) {
        }

    public:

        /**
         * @fn  VType TokenBase::Value() const
         *
         * @brief   Metoda zwaracaj�ca warto��
         */
        VType value() const { return _value; }

        /**
         * @fn  std::string TokenBase::symbol()
         *
         * @brief   Zwraca symbol okre�laj�cy dany token
         * 
         * @return  A std::string.
         */
        std::string symbol() const override { return _symbol; }

        /**
         * @fn  std::string TokenBase::compilerValue()
         *
         * @brief   Zwraca warto�� kompilatora         
         */
        virtual std::string compilerValue() const override { return symbol(); }

        /**
         * @fn  virtual std::string TokenBase::stringValue() const = 0;
         *
         * @brief   Przechowywana warto�� jako string
         */
        virtual std::string stringValue() const = 0;

        /**
        * @fn  virtual std::string TokenBase::toString() const = 0;
        *
        * @brief   Jako string
        */
        virtual std::string toString() const override { return stringValue(); }

        /**
         * @fn  virtual bool TokenBase::isNumber() const override
         *
         * @brief   Czy dany token przechowuje cyfr�
         */
        virtual bool isNumber() const override { return false; }

        /**
         * @fn  virtual bool TokenBase::isOperator() const override
         *
         * @brief   Czy dany token przechowuje operator
         */
        virtual bool isOperator() const override { return false; }

        /**
         * @fn  int TokenBase::priority() const override
         *
         * @brief   Priorytet tokenu - g��wnie dla operator�w
         */
        int priority() const override { return _priority; }
        
    protected:

        /**
        * @fn  void TokenBase::SetValue(VType newValue)
        *
        * @brief   Metoda ustawiaj�ca warto��
        */
        void setValue(VType newValue) { _value = newValue; }

        /**
         * @fn  void TokenBase::setSymbol(std::string newSymbol)
         *
         * @brief   Metoda ustawiaj�ca nowy symbol kompilatora dla danego tokenu
         */
        void setSymbol(const std::string &newSymbol) { _symbol = newSymbol;  }

        /**
         * @fn  void TokenBase::setPriority(const int &newPriority)
         *
         * @brief   Metoda ustawiaj�ca priorytet
         */
        void setPriority(const int &newPriority) { _priority = newPriority; }
        
    };

}

ME_END_NAMESPACE_BASE

#endif /*__ME_BASE_TOKENS_TOKENBASE_H__*/