#ifndef __ME_BASE_TOKENS_ENTITIES_OPERATOR_TDIV_H__
#define __ME_BASE_TOKENS_ENTITIES_OPERATOR_TDIV_H__

#include "../../../StdAfx.h"

#include "../TOperator.h"

ME_BEGIN_NAMESPACE_BASE

namespace Tokens {

    namespace Entities {

        namespace Operators {

            /**
            * @class   TDiv
            *
            * @brief   Klasa tokenu dzielenia
            */
            class TDiv : public TOperator {
            public:

                /**
                * @fn  TDiv::TDiv(std::string value)
                *
                * @brief   Konstruktor
                */
                TDiv() : TOperator("/", 2) {
                    setSymbol("div");
                    setPriority(2);
                }

            public:
                /**
                * @fn  int TDiv::calculate(const int &left) override;
                *
                * @brief   Oblicza warto�� na podstawie operatora
                */
                int calculate(const int &left) override {
                    checkCalculateArguments(1);

                    return 0;
                }

                /**
                * @fn  int TDiv::calculate(const int &left, const int &right) override;
                *
                * @brief   Oblicza warto�� na podstawie operatora
                */
                int calculate(const int &left, const int &right) override {
                    checkCalculateArguments(2);

                    return right / left;
                }

            };

        }

    }

}

ME_END_NAMESPACE_BASE

#endif /*__ME_BASE_TOKENS_ENTITIES_OPERATOR_TDIV_H__*/