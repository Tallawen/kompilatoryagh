#ifndef __ME_BASE_TOKENS_ENTITIES_OPERATOR_TMUL_H__
#define __ME_BASE_TOKENS_ENTITIES_OPERATOR_TMUL_H__

#include "../../../StdAfx.h"

#include "../TOperator.h"

ME_BEGIN_NAMESPACE_BASE

namespace Tokens {

    namespace Entities {

        namespace Operators {

            /**
            * @class   TMul
            *
            * @brief   Klasa tokenu mno�enia
            */
            class TMul : public TOperator {
            public:

                /**
                * @fn  TMul::TMul(std::string value)
                *
                * @brief   Konstruktor
                */
                TMul() : TOperator("*", 2) {
                    setSymbol("mul");
                    setPriority(2);
                }

            public:
                /**
                * @fn  int TMul::calculate(const int &left) override;
                *
                * @brief   Oblicza warto�� na podstawie operatora
                */
                int calculate(const int &left) override {
                    checkCalculateArguments(1);

                    return 0;
                }

                /**
                * @fn  int TMul::calculate(const int &left, const int &right) override;
                *
                * @brief   Oblicza warto�� na podstawie operatora
                */
                int calculate(const int &left, const int &right) override {
                    checkCalculateArguments(2);

                    if (left == 0)
                        throw "Dzielenie przez zero";

                    return right * left;
                }

            };

        }

    }

}

ME_END_NAMESPACE_BASE

#endif /*__ME_BASE_TOKENS_ENTITIES_OPERATOR_TMUL_H__*/