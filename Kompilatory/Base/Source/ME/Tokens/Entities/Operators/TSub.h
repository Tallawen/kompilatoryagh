#ifndef __ME_BASE_TOKENS_ENTITIES_OPERATOR_TSUB_H__
#define __ME_BASE_TOKENS_ENTITIES_OPERATOR_TSUB_H__

#include "../../../StdAfx.h"

#include "../TOperator.h"

ME_BEGIN_NAMESPACE_BASE

namespace Tokens {

    namespace Entities {

        namespace Operators {

            /**
            * @class   TSub
            *
            * @brief   Klasa tokenu odejmowania
            */
            class TSub : public TOperator {
            public:

                /**
                * @fn  TSub::TSub(std::string value)
                *
                * @brief   Konstruktor
                */
                TSub() : TOperator("-", 2) {
                    setSymbol("sub");
                    setPriority(1);
                }

            public:
                /**
                * @fn  int TSub::calculate(const int &left) override;
                *
                * @brief   Oblicza warto�� na podstawie operatora
                */
                int calculate(const int &left) override {
                    checkCalculateArguments(1);

                    return 0;
                }

                /**
                * @fn  int TSub::calculate(const int &left, const int &right) override;
                *
                * @brief   Oblicza warto�� na podstawie operatora
                */
                int calculate(const int &left, const int &right) override {
                    checkCalculateArguments(2);

                    return right - left;
                }

            };

        }

    }

}

ME_END_NAMESPACE_BASE

#endif /*__ME_BASE_TOKENS_ENTITIES_OPERAiTokenBaseTOR_TSUB_H__*/