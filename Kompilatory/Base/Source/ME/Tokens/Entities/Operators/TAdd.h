#ifndef __ME_BASE_TOKENS_ENTITIES_OPERATOR_TADD_H__
#define __ME_BASE_TOKENS_ENTITIES_OPERATOR_TADD_H__

#include "../../../StdAfx.h"

#include "../TOperator.h"

ME_BEGIN_NAMESPACE_BASE

namespace Tokens {

    namespace Entities {

        namespace Operators {

            /**
            * @class   TAdd
            *
            * @brief   Klasa tokenu dodwania
            */
            class TAdd : public TOperator {          
            public:

                /**
                * @fn  TOperator::TOperator(std::string value)
                *
                * @brief   Konstruktor
                */
                TAdd() : TOperator("+", 2) {
                    setSymbol("add");
                    setPriority(1);                    
                }

            public:
                /**
                * @fn  int TAdd::calculate(const int &left) override;
                *
                * @brief   Oblicza warto�� na podstawie operatora
                */
                int calculate(const int &left) override {
                    checkCalculateArguments(1);

                    return 0;
                }

                /**
                * @fn  int TAdd::calculate(const int &left, const int &right) override;
                *
                * @brief   Oblicza warto�� na podstawie operatora
                */
                int calculate(const int &left, const int &right) override {
                    checkCalculateArguments(2);

                    return right + left;
                }

            };

        }

    }

}

ME_END_NAMESPACE_BASE

#endif /*__ME_BASE_TOKENS_ENTITIES_OPERATOR_TADD_H__*/