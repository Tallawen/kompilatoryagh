#ifndef __ME_BASE_TOKENS_ENTITIES_TOPERATOR_H__
#define __ME_BASE_TOKENS_ENTITIES_TOPERATOR_H__

#include "../../StdAfx.h"

#include "../TokenBase.h"

ME_BEGIN_NAMESPACE_BASE

namespace Tokens {

    namespace Entities {
     
        /**
        * @class   TOperator
        *
        * @brief   Klasa tokenu operatora
        */
        class TOperator : public TokenBase<std::string> {
        private:
            int _arguments; /* Ilo�� argument�w jak� potrzebuje dany operator */

        public:

            /**
            * @fn  TOperator::TOperator(std::string value)
            *
            * @brief   Konstruktor
            */
            TOperator(const std::string &value, int arguments = 1) 
                : TokenBase(value), _arguments(arguments) {

            }

        public:
            /**
            * @fn  virtual std::string TOperator::stringValue() const override
            *
            * @brief   Zwraca warto�� jako string
            */
            virtual std::string stringValue() const override { return value(); }

            /**
             * @fn  bool TOperator::isOperator() const override
             *
             * @brief   Czy dany token przechowuje operator
             */
            bool isOperator() const override { return true; }

            /**
             * @fn  int TOperator::arguments() const
             *
             * @brief   Zwraca ilo�� argument�w
             */
            int arguments() const { return _arguments; }

            /**
             * @fn  virtual int TOperator::calculate(const int &left) = 0;
             *
             * @brief   Oblicza warto�� na podstawie operatora
             */
            virtual int calculate(const int &left) = 0;

            /**
             * @fn  virtual int TOperator::calculate(const int &left, const int &right) = 0;
             *
             * @brief   Oblicza warto�� na podstawie operatora
             */
            virtual int calculate(const int &left, const int &right) = 0;

        protected:

            /**
             * @fn  void TOperator::checkCalculateArguments()
             *
             * @brief   Sprawdza ilo�c potrzebnych argument�w, je�li sie nie zgadza wali wyj�tkiem
             */
            void checkCalculateArguments(const int &arg) {
                if (arg != arguments())
                    throw "Ilo�� argument�w dla operatora \"" + stringValue() + "\" jest niepoprawna";
            }

            

        };

    }

}

ME_END_NAMESPACE_BASE

#endif /*__ME_BASE_TOKENS_ENTITIES_TOPERATOR_H__*/