#ifndef __ME_BASE_TOKENS_ENTITIES_TNUMBER_H__
#define __ME_BASE_TOKENS_ENTITIES_TNUMBER_H__

#include "../../StdAfx.h"

#include "../TokenBase.h"

ME_BEGIN_NAMESPACE_BASE

namespace Tokens {

    namespace Entities {

        /**
         * @class   TNumber
         *
         * @brief   Klasa tokenu warto�ci numerycznej
         */
        class TNumber : public TokenBase<int> {
        public:

            /**
             * @fn  TNumber::TNumber(double value)
             *
             * @brief   Konstruktor
             */
            TNumber(const double &value) : TokenBase(value) {
                setSymbol("put");
            }

            /**
             * @fn TNumber::TNumber(std::string strValue)
             *
             * @brief  Konstruktor
             */
            TNumber(const std::string &strValue) : TNumber(0) {
                double val = std::stod(strValue);

                setValue(val);
            }

        public:

            /**
            * @fn  std::string TokenBase::compilerValue()
            *
            * @brief   Zwraca warto�� kompilatora
            */
            std::string compilerValue() const override { return symbol() + " " + std::to_string(value()); }

            /**
             * @fn  virtual std::string TNumber::stringValue() const override
             *
             * @brief   Zwraca warto�� jako string             
             */
            virtual std::string stringValue() const override { return std::to_string(value()); }

            /**
             * @fn  bool TNumber::isNumber() const override
             *
             * @brief   Czy dany token przechowuje cyfr�
             */
            bool isNumber() const override { return true; }

        };

    }

}

ME_END_NAMESPACE_BASE

#endif /*__ME_BASE_TOKENS_ENTITIES_TNUMBER_H__*/