#ifndef __ME_BASE_TOKENS_ENTITIES_TEND_H__
#define __ME_BASE_TOKENS_ENTITIES_TEND_H__

#include "../../StdAfx.h"

#include "../TokenBase.h"

ME_BEGIN_NAMESPACE_BASE

namespace Tokens {

    namespace Entities {

        /**
        * @class   TEnd
        *
        * @brief   Klasa tokenu ko�ca
        */
        class TEnd : public TokenBase<int> {
        public:

            /**
            * @fn  TNumber::TNumber(double value)
            *
            * @brief   Konstruktor
            */
            TEnd() : TokenBase("") {
                setSymbol("end");
            }         

        };

    }

}

ME_END_NAMESPACE_BASE

#endif /*__ME_BASE_TOKENS_ENTITIES_TEND_H__*/