#ifndef __ME_BASE_TOKENS_ITOKENBASE_H__
#define __ME_BASE_TOKENS_ITOKENBASE_H__

#include "../StdAfx.h"

ME_BEGIN_NAMESPACE_BASE

namespace Tokens {

    /**
     * @class   ITokenBase
     *
     * @brief   Interfejs token�w
     */
    class ITokenBase {
    protected:

        /**
         * @fn  ITokenBase::ITokenBase()
         *
         * @brief   Konstruktor
         *          
         * Nie mo�na utwo�y� obiektu tej klasy (jest to tylko interfejs          *
         */
        ITokenBase() {
        }

    public:
        /**
         * @fn  virtual std::string ITokenBase::stringValue() const = 0;
         *
         * @brief   Przechowywana warto�� jako string
         */
        virtual std::string stringValue() const = 0;

        /**
        * @fn  virtual std::string ITokenBase::toString() const = 0;
        *
        * @brief   Jako string
        */
        virtual std::string toString() const = 0;

        /**
        * @fn  std::string TokenBase::symbol()
        *
        * @brief   Zwraca symbol okre�laj�cy dany token
        *
        * @return  A std::string.
        */
        virtual std::string symbol() const = 0;

        /**
        * @fn  std::string TokenBase::compilerValue()
        *
        * @brief   Zwraca warto�� kompilatora
        */
        virtual std::string compilerValue() const = 0;

        /**
         * @fn  virtual bool ITokenBase::isNumber() const = 0;
         *
         * @brief   Czy dany token przechowuje cyfr�
         */
        virtual bool isNumber() const = 0;

        /**
         * @fn  virtual bool ITokenBase::isOperator() const = 0;
         *
         * @brief   Czy dany token przechowuje operator
         */
        virtual bool isOperator() const = 0;

        /**
         * @fn  virtual int ITokenBase::priority() const = 0;
         *
         * @brief   Priorytet tokenu - g��wnie dla operator�w
         */
        virtual int priority() const = 0;

    };

}

ME_END_NAMESPACE_BASE

#endif /*__ME_BASE_TOKENS_ITOKENBASE_H__*/
