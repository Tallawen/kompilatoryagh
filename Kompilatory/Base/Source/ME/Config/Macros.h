#ifndef __ME_BASE_CONFIG_MACROS__
#define __ME_BASE_CONFIG_MACROS__

#include <cassert>

#define ME_NAMESPACE ME
#define ME_PREPEND_NAMESPACE(Name) ::ME_NAMESPACE::Name
#define ME_USE_NAMESPACE using namespace ::ME_NAMESPACE;
#define ME_BEGIN_NAMESPACE namespace ME_NAMESPACE {
#define ME_BEGIN_PREPEND_NAMESPACE(Name) namespace Name {
#define ME_END_NAMESPACE }

#define ME_BEGIN_NAMESPACE_BASE ME_BEGIN_NAMESPACE \
    ME_BEGIN_PREPEND_NAMESPACE(Base)

#define ME_END_NAMESPACE_BASE  ME_END_NAMESPACE \
    ME_END_NAMESPACE

#define ME_BEGIN_NAMESPACE_COMPILER ME_BEGIN_NAMESPACE \
    ME_BEGIN_PREPEND_NAMESPACE(Compiler)

#define ME_END_NAMESPACE_COMPILER  ME_END_NAMESPACE \
    ME_END_NAMESPACE

inline void me_noop(void) {}

#define ME_NO_DEBUG

#ifndef ME_ASSERT
#ifndef ME_NO_DEBUG
#define ME_ASSERT(Cond) me_noop()
#else
#define ME_ASSERT(Cond) ((!(Cond))) ? assert(false) : me_noop()
#endif
#endif

#define ME_DELETE(x) do { delete (x); (x) = nullptr; } while(0)

#define ME_VERSION_CHECK(major, minor, patch) ((major<<16)|(minor<<8)|(patch))

#endif /*__ME_BASE_CONFIG_MACROS__*/

