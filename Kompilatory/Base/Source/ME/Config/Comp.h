#ifndef __ME_BASE_CONFIG_COMP_H__
#define __ME_BASE_CONFIG_COMP_H__

/*
*  Czy budujemy bibliotekę, czy likujemy
*
* Kolejność budowania
*/
#define ME_LIB_BASE_BUILD     0


/* Rozpoznawanie systemu
*/
#if defined(_WIN32) || defined(__WIN32__)
// Windows
#define ME_SYSTEM_WINDOWS

#elif defined(linux) || defined(__linux)
// Linux
#define ME_SYSTEM_LINUX

#else
// Inny
#error This operating system is not supported by LOGGER library

#endif

/* Deklaracje exportu i importu w zależności od systemu
*/
#ifdef ME_SYSTEM_WINDOWS
#define ME_DECL_EXPORT  //__declspec(dllexport)
#define ME_DECL_IMPORT  //__declspec(dllimport)
#define ME_DECL_HIDDEN

#elif defined(PB_SYSTEM_LINUX)
#define ME_DECL_EXPORT  __attribute__((visibility("default")))
#define ME_DECL_IMPORT  __attribute__((visibility("default")))
#define ME_DECL_HIDDEN  __attribute__((visibility("hidden")))

#else
#define ME_DECL_EXPORT
#define ME_DECL_IMPORT
#define ME_DECL_HIDDEN

#endif


#if ME_LIB_BASE_BUILD == 1
#define ME_LIB_BASE_API       ME_DECL_EXPORT
#else
#define ME_LIB_BASE_API       ME_DECL_IMPORT
#endif

#endif /*__ME_BASE_CONFIG_COMP_H__*/

