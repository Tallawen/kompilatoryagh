#ifndef __ME_BASE_CONFIG_GLOBAL_H__
#define __ME_BASE_CONFIG_GLOBAL_H__

#include "Macros.h"

#include <vector>

ME_BEGIN_NAMESPACE_BASE

namespace Tokens {
    class ITokenBase;
}

typedef std::vector<Tokens::ITokenBase*> TokensVector;

ME_END_NAMESPACE_BASE

#endif /*__ME_BASE_CONFIG_GLOBAL_H__*/