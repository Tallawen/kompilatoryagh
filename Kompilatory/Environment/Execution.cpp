#include "stdafx.h"
#include "Execution.h"
#include "Utils.h"


int Execution::execute(std::vector<std::string> commands)
{
	for each (auto var in commands) {
		if (!executeLine(var))
			break;
	}
	if (_numbers.size() > 1)
		throw "Cos poszlo nie tak, nieprawidlowy rozmiar stosu";
	return _numbers.top();
}

bool Execution::executeLine(std::string & line) {
	auto tokens = Utils::split(line, " ");
	if ((tokens[0] != "put" && tokens.size() == 2) || (tokens.size() > 2))
		throw "Nieprawidlowa instrukcja";
	if (tokens[0] == "put") {
		auto num = Utils::stringToInt(tokens[1]);
		_numbers.push(num);
		return true;
	}
	if (tokens[0] == "add") {
		if (_numbers.size() < 2)
			throw "Za malo liczb";
		auto second = _numbers.top();
		_numbers.pop();
		auto first = _numbers.top();
		_numbers.pop();
		auto result = first + second;
		_numbers.push(result);
		return true;
	}
	if (tokens[0] == "sub") {
		if (_numbers.size() < 2)
			throw "Za malo liczb";
		auto second = _numbers.top();
		_numbers.pop();
		auto first = _numbers.top();
		_numbers.pop();
		auto result = first - second;
		_numbers.push(result);
		return true;
	}
	if (tokens[0] == "mul") {
		if (_numbers.size() < 2)
			throw "Za malo liczb";
		auto second = _numbers.top();
		_numbers.pop();
		auto first = _numbers.top();
		_numbers.pop();
		auto result = first * second;
		_numbers.push(result);
		return true;
	}
	if (tokens[0] == "div") {
		if (_numbers.size() < 2)
			throw "Za malo liczb";
		auto second = _numbers.top();
		_numbers.pop();
		auto first = _numbers.top();
		_numbers.pop();
		auto result = first / second;
		_numbers.push(result);
		return true;
	}
	if (tokens[0] == "end") {
		return false;
	}
}