#ifndef __UTILS_H__
#define __UTILS_H__

#include <string>
#include <vector>
#include <sstream>

namespace Utils {

	std::vector<std::string> split(std::string line, std::string delimiter) {
		size_t pos = 0;
		std::string token;
		std::vector<std::string> tokens;
		while ((pos = line.find(delimiter)) != std::string::npos) {
			token = line.substr(0, pos);
			if (!token.empty())
				tokens.push_back(token);
			line.erase(0, pos + delimiter.length());
		}
		tokens.push_back(line);

		return tokens;
	}

	int stringToInt(const std::string & string) {
		std::istringstream iss(string);
		int num;
		iss >> num;
		return num;
	}
}

#endif
