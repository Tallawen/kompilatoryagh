#ifndef __EXECUTION_H__
#define __EXECUTION_H__

#include "stdafx.h"

class Execution
{
private:
	std::stack<int> _numbers;

public:
	int execute(std::vector<std::string>);

private:
	bool executeLine(std::string&);

};

#endif