#include "stdafx.h"
#include "Execution.h"

std::string trim(const std::string& str)
{
	if (str.empty())
		return "";
	size_t first = str.find_first_not_of(' ');
	size_t last = str.find_last_not_of(' ');
	return str.substr(first, (last - first + 1));
}

int main(int argc, char * argv[]) {
	try {

		std::vector<std::string> commands;
		if (argc == 1) {
			std::cout << "Oczekiwanie na kod:" << std::endl;
			for (;;) {
				std::string line;
				std::getline(std::cin, line);
				line = trim(line);
				if (line.empty())
					break;
				commands.push_back(line);
			}
		}
		else
		{
			std::string path = argv[1];
			for (int i = 2; i < argc; i++)
				path += ' ' + argv[i];
			std::ifstream file(path, std::ios::binary);
			while (!file.eof()) {
				std::string line;
				std::getline(file, line);
				line = trim(line);
				if (line.empty())
					continue;
				commands.push_back(line);
			}
		}

		Execution exec;
		std::cout << exec.execute(commands);
	} 
	catch (std::string message) {
		std::cerr << "Wyjatek: " << message << std::endl;
		return EXIT_FAILURE;
	}
	catch (...) {
		std::cerr << "Nieznany wyjatek\n";
		return EXIT_FAILURE;
	}
}