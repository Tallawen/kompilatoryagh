#include <iostream>
#include <Tokens\Entities\TNumber.h>
#include <Tokens\Entities\Operators\TAdd.h>
#include <Tokens\Entities\Operators\TSub.h>
#include <Tokens\Entities\Operators\TDiv.h>
#include <Tokens\Entities\Operators\TMul.h>

#include "Source\Scanner\Scanner.h"

#include <Tree\ExpressionTree.h>
#include <vector>

using namespace ME::Base::Tokens;

int main(int argc, char * argv[]) {
	try {
		std::string input;

		if (argc == 1) {
			std::cout << "Wyrazenie: ";
			std::getline(std::cin, input);
		}
		else
		{
			input = argv[1];
			for (int i = 2; i < argc; i++) {
				input += ' ';
				input += argv[i];
			}
			if (input[0] == '\'') {
				if (input[input.length() - 1] != '\'')
					throw "Nieprawidlowy argument";
				input = input.substr(1, input.length() - 2);
			}
			else
			{
				std::ifstream file(input);
				if (!file.is_open())
					throw "Blad w otwarciu pliku";
				std::getline(file, input);
				file.close();
			}
		}

		ME::Compiler::Scanner::Scanner scanner2(input);
		scanner2.scane();

		ME::Base::Tree::ExpressionTree tree_3(scanner2.tokens());
		tree_3.build();

		auto sequence = tree_3.getSequence();

		for each (auto var in sequence) {
			std::cout << var->compilerValue() << std::endl;
		}

		std::fstream output("a.out", std::ios::out | std::ios::binary | std::ios::trunc);

		for each (auto var in sequence) {
			output << var->compilerValue() << std::endl;
		}

		output.close();

	}
	catch (std::string message) {
		std::cerr << "Wyjatek: " << message << std::endl;
		return EXIT_FAILURE;
	}
	catch (...) {
		std::cerr << "Nieznany wyjatek\n";
		return EXIT_FAILURE;
	}
}