#include "Scanner.h"

#include <Tokens\ITokenBase.h>
#include <Tokens\TokenMapper.h>

#include <regex>
#include <iostream>
#include <sstream>

ME_BEGIN_NAMESPACE_COMPILER

namespace Scanner {

    /************************************************/
    bool Scanner::valid(const bool &throwEx) {  
        std::regex regex("([-+]?[0-9]*\\.?[0-9]+[\\/\\+\\-\\*])+([-+]?[0-9]*\\.?[0-9]+)");

        bool ret = std::regex_match(_string, regex);
        
        if(!ret && throwEx)
            throw "Excep";

        return ret;
    }

    /************************************************/
    Base::TokensVector Scanner::scane() {
        _tokens.clear();

        valid();
        
        std::regex regex(_splitRegex, std::regex_constants::icase);

        std::regex_iterator<std::string::iterator> it(_string.begin(), _string.end(), regex);
        std::regex_iterator<std::string::iterator> end;
                
        while (it != end) {
            ME::Base::Tokens::ITokenBase *iTokenBase = ME::Base::Tokens::TokenMapper::FromString(it->str());
            
            if (iTokenBase == nullptr)
                throw "Kompilacja nie powiod�a si�, wykryto b��dny format danych";
            
            _tokens.push_back(iTokenBase);
            
            ++it;
        }

        return _tokens;
    }

    /************************************************/
    Base::TokensVector Scanner::scane(const std::string &string) {
        setString(string);

        return scane();   
    }

    /************************************************/
    void Scanner::removeStringWhitespace() {
        _string.erase(remove_if(_string.begin(), _string.end(), ::isspace), _string.end());
    }

    /************************************************/
    void Scanner::setString(const std::string &string) {
        _string = string;

        removeStringWhitespace();
    }
}

ME_END_NAMESPACE_COMPILER