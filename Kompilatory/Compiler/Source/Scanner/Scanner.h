#ifndef __ME_COMPILER_SCANNER_SCANNER_H__
#define __ME_COMPILER_SCANNER_SCANNER_H__

#include "../StdAfx.h"

ME_BEGIN_NAMESPACE_BASE

namespace Tokens {
    class ITokenBase;
}

ME_END_NAMESPACE_BASE

ME_BEGIN_NAMESPACE_COMPILER

namespace Scanner {

    class Scanner {
    public:
        std::string _string; /* String do skanowania (wej�ciowy) */
        
        Base::TokensVector _tokens; /* Lista token�w kt�re zosta�y wygenerowane */

        std::string _splitRegex = "(\\d+)|[\\-\\+\\*\\/]"; /* Wyra�enie regularne odno�nie grupowania*/
        
    public:

        /**
         * @fn  Scanner::Scanner()
         *
         * @brief   Konstruktor
         */
        Scanner() : _string("") {            
        }

        /**
         * @fn  Scanner::Scanner(const std::string &string)
         *
         * @brief   Konstruktor
         */
        Scanner(const std::string &string) {
            setString(string);
        }

    public:

        /**
         * @fn  bool Scanner::ValidString(bool throwEx = false);
         *
         * @brief   Metoda sprawdza tekst (czy wyra�enie jest dobre gramatycznie) 
         *                   
         * @param   throwEx czy rzuci� wyj�tek
         */
        bool valid(const bool &throwEx = false); 

        /**
         * @fn  TokensVector Scanner::scane();
         *
         * @brief   Metoda skanuj�ca tekst i generuj�ca na jego podstawie tokeny
         */
        Base::TokensVector scane();

        /**
         * @fn  TokensVector Scanner::scane(const std::string &string);
         *
         * @brief   Metoda skanuj�ca tekst i generuj�ca na jego podstawie tokeny
         */
        Base::TokensVector scane(const std::string &string);

        /**
         * @fn  TokensVector Scanner::tokens() const;
         *
         * @brief   Metoda zwracaj�ca list� wygenerowanych token�w
         */
        Base::TokensVector tokens() const { return _tokens; }

        /**
         * @fn  std::string Scanner::string() const
         *
         * @brief   Metoda zwraca skanowany tekst
         */
        std::string string() const { return _string; }

        /**
         * @fn  static TokensVector Scanner::scane(const std::string &string)
         *
         * @brief   Statyczna metoda skanuj�ca
         */
        static Base::TokensVector Scane(const std::string &string) {
            Scanner obj(string);

            return obj.scane();
        }

    private:

        /**
         * @fn  void Scanner::removeStringWhitespace();
         *
         * @brief   Usuwa bia�e znaki z tekstu
         */
        void removeStringWhitespace();

        /**
         * @fn  void Scanner::setString(const std::string &string);
         *
         * @brief   Metoda ustawiaj�ca string
         */
        void setString(const std::string &string);

    };

}

ME_END_NAMESPACE_COMPILER

#endif /*__ME_COMPILER_SCANNER_SCANNER_H__*/