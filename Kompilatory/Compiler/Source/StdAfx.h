#ifndef __ME_COMPILER_STDAFX_H__
#define __ME_COMPILER_STDAFX_H__

#include "Config/Macros.h"
#include "Config/Comp.h"
#include "Config/Global.h"

#include <string>
#include <vector>
#include <algorithm>
#include <locale>
#include <cctype>
#include <stack>
#include <iostream>
#include <fstream>

#endif /*__ME_COMPILER_STDAFX_H__*/